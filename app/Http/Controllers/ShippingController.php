<?php

namespace App\Http\Controllers;
use Storage;

class ShippingController extends Controller {
	private $jsonProvince;

	public function __construct() {
		$this->jsonProvince = Storage::disk('local')->get('public/json/province.json');
	}
	//

	public function checkCity() {

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: de9f095b6930016a15eba1cd42fee322",
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		$html = "";
		$html .= "<select class='form-control' name='provinsi' id='provinsi' required=''>";
		$html .= "<option value='' style='color:silver'>Pilih Provinsi Tujuan</option>";
		$data = json_decode($response, true);
		for ($i = 0; $i < count($data['rajaongkir']['results']); $i++) {
			$html .= "<option value='" . $data['rajaongkir']['results'][$i]['province_id'] . "' name='" . $data['rajaongkir']['results'][$i]['province'] . "'>" . $data['rajaongkir']['results'][$i]['province'] . "</option>";
		}
		$html .= "</select><br>";
		return $html;
	}
	public function checkProvince() {

		$dataProvince = json_decode($this->jsonProvince, true);
		//print_r($dataProvince);
		return $dataProvince;
		/*$num = 0;
			foreach ($dataProvince as $row) {
				echo $dataProvince[$num++]['province'];
		*/

	}
}

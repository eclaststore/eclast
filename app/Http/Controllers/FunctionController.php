<?php
namespace App\Http\Controllers;

use App\Http\Traits\CartTrait;
use App\Http\Traits\ShippingTrait;
use Illuminate\Http\Request;
use Mail;

class FunctionController extends Controller {

	use ShippingTrait;

	use CartTrait;

	public function CART__add($price, $id, $name, $picture) {

		return $this->addToCart($price, $id, $name, $picture);

	}

	public function CART__remove($rowId) {

		return $this->remove($rowId);

	}

	public function CART__emptyCart() {

		return $this->destroy();

	}

	public function ONGKIR__listCity(Request $request) {

		return $this->getCity($request->province_id);

	}

	public function ONGKIR__cost(Request $request) {

		return $this->getOngkir($request->city_id);

	}

	public static function toRupiah($angka) {
		$hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
		return $hasil_rupiah;

	}

	public function MAIL__send($getEmail, $productCart, $subTotal, $getOngkir, $getCity, $uniqueCode) {
		/*	try {
			} catch (Exception $e) {
				return response(['status' => false, 'errors' => $e->getMessage()]);
		*/

		$total = 0;
		$total = $subTotal + $getOngkir + $uniqueCode;

		$data = array('productCart' => $productCart, 'ongkir' => $getOngkir, 'subTotal' => $subTotal, 'city' => $getCity, 'kodeUnik' => $uniqueCode, 'total' => $total);

		Mail::send('mail', $data, function ($message) use ($getEmail) {
			$message->to($getEmail, 'Eclast Invoice')->subject
				('Eclast Store Invoice');
			$message->from('bantuan@eclast-store.com', 'Eclast Store');
		});
		echo "Basic Email Sent. Check your inbox.";

	}

	public function ORDER__store() {
		echo "it works";
	}

	public function TELEGRAM__invoiceNotif($name, $subTotal, $productCart) {
		/*	 	$total = 0;
			$total = $subTotal + $getOngkir + $uniqueCode;*/

		$data = json_decode($productCart);
		$productBuy = "";

		foreach ($data as $v) {
			$productBuy .= ucwords(strtolower($v->name)) . ' ' . self::toRupiah($v->price) . ' ';

		}
		/*echo $productBuy;
		die;*/

		$text = 'Terima kasih atas pemesanan ' . $productBuy . '  untuk pembayaran bisa di transfer di rekening BCA atas nama Denta Zamzam Yasin Muhajir dengan nominal Rp ' . self::toRupiah($subTotal) . '.setelah bayar kamu bisa konfirmasi lewat SMS/WA ke nomer ini dengan memberikan info no order dan nominal total transfer. Sekian Makasih';

		$url = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text=' . $text . '&parse_mode=html';
		return file_get_contents($url);

	}

}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'PageController@homepage')->name('homepage');

Route::get('/home', 'PageController@index');

Route::get('/keranjang', 'PageController@cart');

Route::get('/merch/{merch_name}', 'PageController@category');

Route::get('/detail/{slug}', 'PageController@detail');

Route::any('/add/cart/{harga}/{id}/{name}/{picture}', 'FunctionController@CART__add');

Route::any('/cart/remove/{rowId}/', 'FunctionController@CART__remove');

Route::any('/cart/destroy/', 'FunctionController@CART__emptyCart');

Route::any('/store/form/', 'FunctionController@ORDER__store');
